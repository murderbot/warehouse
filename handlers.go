package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/flaviamissi/warehouse/db"
	"github.com/flaviamissi/warehouse/inventory"
	"github.com/flaviamissi/warehouse/product"
)

type getProductsResp struct {
	ID       string         `json:"id"`
	Name     string         `json:"name"`
	Articles []articlesResp `json:"articles"`
}

type articlesResp struct {
	Name     string `json:"name"`
	Quantity int32  `json:"quantity"`
	Stock    int64  `json:"stock"`
}

func sellProduct(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	database := db.FromContext(ctx)
	coll := database.Collection(product.CollectionName)
	vars := mux.Vars(req)
	id, err := primitive.ObjectIDFromHex(vars["id"])
	if err != nil {
		log.Errorf("Failed parsing product id '%v': %v", vars["id"], err)
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"error": "Product id must be an integer."}`)
		return
	}
	product := &product.Product{}
	err = coll.FindOne(ctx, bson.M{"_id": id}).Decode(&product)
	if err != nil {
		data := `{"error": "Error loading product from data store."}`
		statusCode := http.StatusInternalServerError
		if err == mongo.ErrNoDocuments {
			data = `{"error": "Product not found."}`
			statusCode = http.StatusNotFound
		}
		log.Errorf("Failed to fetch product from database: %v", err)
		w.WriteHeader(statusCode)
		fmt.Fprintf(w, data)
		return
	}

	if err := product.Sell(ctx, database); err != nil {
		log.Errorf("Failed to sell product: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, `{"error": "Could not sell product."}`)
		return
	}
}

func getProducts(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	database := db.FromContext(ctx)
	coll := database.Collection(product.CollectionName)

	w.Header().Set("Content-Type", "application/json")

	lookup := bson.D{{"$lookup", bson.D{
		{"from", inventory.CollectionName},
		{"localField", "articles.external_id"},
		{"foreignField", "external_id"},
		{"as", "inventory"},
	}}}
	project := bson.D{{"$project",
		bson.M{
			"name":              1,
			"inventory.name":    1,
			"inventory.stock":   1,
			"articles.quantity": 1,
		},
	}}

	cur, err := coll.Aggregate(ctx, mongo.Pipeline{lookup, project})
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, `{"error": "Could not list products."}`)
		return
	}
	var loaded []bson.M
	if err := cur.All(ctx, &loaded); err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, `{"error": "Could not list products."}`)
		return
	}

	data := []getProductsResp{}
	for _, m := range loaded {
		id := m["_id"].(primitive.ObjectID)
		arts := m["articles"].(bson.A)
		p := getProductsResp{
			ID:       id.Hex(),
			Name:     m["name"].(string),
			Articles: []articlesResp{},
		}
		for i, art := range arts {
			a := art.(bson.M)
			inv := m["inventory"].(bson.A)[i].(bson.M)
			p.Articles = append(p.Articles,
				articlesResp{
					Name:     inv["name"].(string),
					Stock:    inv["stock"].(int64),
					Quantity: a["quantity"].(int32),
				})
		}
		data = append(data, p)
	}

	if err := json.NewEncoder(w).Encode(data); err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, `{"error": "Could not list products."}`)
		return
	}
}

func mapArticles(articles []product.ProductArticle) map[int64]product.ProductArticle {
	arts := make(map[int64]product.ProductArticle, len(articles))
	for _, art := range articles {
		arts[art.ExternalID] = art
	}
	return arts
}
