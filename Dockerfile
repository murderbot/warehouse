FROM golang:1.15-alpine3.12 as builder

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 go build

FROM scratch

COPY --from=builder /app/warehouse /app/

EXPOSE 8182
ENTRYPOINT ["/app/warehouse"]
