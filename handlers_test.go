package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/flaviamissi/warehouse/db"
	"github.com/flaviamissi/warehouse/inventory"
	"github.com/flaviamissi/warehouse/product"
)

func TestHandlers(t *testing.T) {
	ctx := context.Background()
	client, err := db.Connect(ctx, "mongodb://localhost")
	if err != nil {
		t.Fatalf("Failed to connect to database: %v", err)
	}
	defer client.Disconnect(ctx)
	database := db.Database(client)

	// seed the database with articles and products
	f, err := os.Open("inventory.json")
	if err != nil {
		t.Fatalf("Failed to open inventory json: %v", err)
	}
	inv, err := inventory.LoadInventory(f)
	if err != nil {
		t.Fatalf("Failed to loading inventory from json: %v", err)
	}
	if err := inventory.SaveMany(ctx, database, inv.Articles); err != nil {
		t.Fatalf("Failed to loading inventory from json: %v", err)
	}
	artIDs := make([]int64, len(inv.Articles))
	for _, article := range inv.Articles {
		artIDs = append(artIDs, article.ExternalID)
	}
	defer database.Collection(inventory.CollectionName).DeleteMany(
		ctx, bson.M{"external_id": bson.M{"$in": artIDs}})

	f, err = os.Open("products.json")
	if err != nil {
		t.Fatalf("Failed to open product json: %v", err)
	}
	products, err := product.LoadProducts(f)
	if err != nil {
		t.Fatalf("Failed to loading product from json: %v", err)
	}
	if err := product.SaveMany(ctx, database, products); err != nil {
		t.Fatalf("Failed to loading product from json: %v", err)
	}
	pIDs := make([]primitive.ObjectID, len(products))
	for _, p := range products {
		pIDs = append(pIDs, p.ID)
	}

	defer database.Collection(product.CollectionName).DeleteMany(
		ctx, bson.M{"_id": bson.M{"$in": pIDs}})

	t.Run("SellProduct", func(t *testing.T) {
		router := mux.NewRouter()
		router.Use(db.MiddlewareFn(database))
		router.HandleFunc("/product/sell/{id:[0-9a-fA-F]+}",
			http.HandlerFunc(sellProduct)).Methods("PATCH")

		t.Run("WithExistingProduct", func(t *testing.T) {
			p := products[0]
			path := fmt.Sprintf("/product/sell/%s", p.ID.Hex())
			r := httptest.NewRequest("PATCH", path, nil)
			rw := httptest.NewRecorder()
			router.ServeHTTP(rw, r)

			result := rw.Result()
			if result.StatusCode != http.StatusOK {
				body, _ := ioutil.ReadAll(result.Body)
				t.Fatalf("Failed to sell product '%s': %s", p.Name, string(body))
			}
		})

		t.Run("WithAbsentProduct", func(t *testing.T) {
			path := fmt.Sprintf("/product/sell/%s", primitive.NewObjectID())
			r := httptest.NewRequest("PATCH", path, nil)
			rw := httptest.NewRecorder()
			router.ServeHTTP(rw, r)

			result := rw.Result()
			if result.StatusCode != http.StatusNotFound {
				t.Fatalf("Did not return %d whith absent product!", result.StatusCode)
			}
		})
	})
	t.Run("GetProducts", func(t *testing.T) {
		router := mux.NewRouter()
		router.Use(db.MiddlewareFn(database))
		router.HandleFunc("/product",
			http.HandlerFunc(getProducts)).Methods("GET")
	})
}
