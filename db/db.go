package db

import (
	"context"
	"net/http"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	dbKey  = 0
	dbName = "warehouse"
)

func Connect(ctx context.Context, dbURI string) (*mongo.Client, error) {
	client, err := mongo.NewClient(options.Client().ApplyURI(dbURI))
	if err != nil {
		return nil, err
	}
	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func Database(client *mongo.Client) *mongo.Database {
	return client.Database(dbName)
}

func MiddlewareFn(db *mongo.Database) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		newCtx := NewContextWithDBFn(db)
		fn := func(rw http.ResponseWriter, req *http.Request) {
			ctx := newCtx(req.Context(), req)
			next.ServeHTTP(rw, req.WithContext(ctx))
		}
		return http.HandlerFunc(fn)
	}
}

func NewContextWithDBFn(db *mongo.Database) func(ctx context.Context, req *http.Request) context.Context {
	return func(ctx context.Context, req *http.Request) context.Context {
		return context.WithValue(ctx, dbKey, db)
	}
}

func FromContext(ctx context.Context) *mongo.Database {
	return ctx.Value(dbKey).(*mongo.Database)
}
