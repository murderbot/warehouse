package inventory

import (
	"context"
	"os"
	"reflect"
	"strings"
	"testing"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/flaviamissi/warehouse/db"
)

func TestLoadInventorySingleArticle(t *testing.T) {
	s := `{"inventory": [{"art_id": "10", "name": "leg", "stock": "12"}]}`
	inventory, err := LoadInventory(strings.NewReader(s))
	if err != nil {
		t.Fatalf("Error loading inventory: %s", err)
	}
	if inventory == nil || len(inventory.Articles) == 0 {
		t.Fatalf("Inventory is empty, but no error was returned")
	}
	art := inventory.Articles[0]
	if art.ExternalID != 10 {
		t.Errorf("Wrong article id, want 10 got '%d'", art.ExternalID)
	}
	if art.Name != "leg" {
		t.Errorf("Wrong article name, want leg got '%s'", art.Name)
	}
	if art.Stock != 12 {
		t.Errorf("Wrong article stock, want 12 got '%d'", art.Stock)
	}
}

func TestLoadInventoryMultipleArticles(t *testing.T) {
	s := `
	{"inventory": [
		{"art_id": "1", "name": "leg", "stock": "12"},
		{"art_id": "2", "name": "seat", "stock": "24"}
	]}`
	inventory, err := LoadInventory(strings.NewReader(s))
	if err != nil {
		t.Fatalf("Error loading inventory: %s", err)
	}
	if inventory == nil || len(inventory.Articles) == 0 {
		t.Fatalf("Inventory is empty, but no error was returned")
	}
	if len(inventory.Articles) != 2 {
		t.Fatalf("Failed to load full inventory, expected 2 articles, got '%d'",
			len(inventory.Articles))
	}

	// asset on individual articles' properties
	leg := inventory.Articles[0]
	if leg.ExternalID != 1 {
		t.Errorf("Wrong article id, want 1 got '%d'", leg.ExternalID)
	}
	if leg.Name != "leg" {
		t.Errorf("Wrong article name, want leg got '%s'", leg.Name)
	}
	if leg.Stock != 12 {
		t.Errorf("Wrong article stock, want 12 got '%d'", leg.Stock)
	}

	seat := inventory.Articles[1]
	if seat.ExternalID != 2 {
		t.Errorf("Wrong article id, want 2 got '%d'", seat.ExternalID)
	}
	if seat.Name != "seat" {
		t.Errorf("Wrong article name, want seat got '%s'", seat.Name)
	}
	if seat.Stock != 24 {
		t.Errorf("Wrong article stock, want 24 got '%d'", seat.Stock)
	}
}

func TestLoadInventoryFromFile(t *testing.T) {
	f, err := os.Open("../inventory.json")
	if err != nil {
		t.Fatalf("Failed to read inventory.json: %s", err)
	}
	defer f.Close()
	inventory, err := LoadInventory(f)
	if err != nil {
		t.Fatalf("Error loading inventory: %s", err)
	}
	if len(inventory.Articles) != 4 {
		t.Fatalf("Wrong number of articles in inventory, want 4, got '%d'",
			len(inventory.Articles))
	}
}

func TestSaveMany(t *testing.T) {
	ctx := context.Background()
	client, err := db.Connect(ctx, "mongodb://localhost")
	if err != nil {
		t.Log("Failed to setup db!")
		t.Fatal(err)
	}
	defer client.Disconnect(ctx)
	database := db.Database(client)
	coll := database.Collection(CollectionName)

	f, err := os.Open("../inventory.json")
	if err != nil {
		t.Fatalf("Failed to read inventory.json: %s", err)
	}
	defer f.Close()
	inventory, err := LoadInventory(f)
	if err != nil {
		t.Fatalf("Error loading inventory: %s", err)
	}

	if err := SaveMany(ctx, database, inventory.Articles); err != nil {
		t.Fatalf("Error saving articles: %v", err)
	}

	ids := []primitive.ObjectID{}
	for _, art := range inventory.Articles {
		ids = append(ids, art.ID)
	}
	filter := bson.M{"_id": bson.M{"$in": ids}}
	defer func() {
		_, err := coll.DeleteMany(ctx, filter)
		if err != nil {
			t.Errorf("Fail to delete articles: %v", filter)
		}
	}()
	cur, err := coll.Find(ctx, filter)
	if err != nil {
		t.Fatalf("Error fetching articles from database: %v", err)
	}
	defer cur.Close(ctx)
	articles := make([]*Article, len(inventory.Articles))
	if err := cur.All(ctx, &articles); err != nil {
		t.Fatalf("Error in cursor: %v", err)
	}
	if err := cur.Err(); err != nil {
		t.Fatalf("Error in cursor: %v", err)
	}

	if !reflect.DeepEqual(articles, inventory.Articles) {
		for _, art := range inventory.Articles {
			t.Logf("Input %v:", art)
		}
		t.Fatal("Inventory from database did not match input.")
	}
}

func TestSaveArticle(t *testing.T) {
	ctx := context.Background()
	client, err := db.Connect(ctx, "mongodb://localhost")
	if err != nil {
		t.Log("Failed to setup db!")
		t.Fatal(err)
	}
	defer client.Disconnect(ctx)
	database := db.Database(client)
	coll := database.Collection(CollectionName)

	t.Run("WithAllFields", func(t *testing.T) {
		art := &Article{
			ExternalID: 1,
			Name:       "screw",
			Stock:      20,
		}
		if err := art.Save(ctx, database); err != nil {
			t.Fatalf("Failed to save article: %v", err)
		}
		defer func(art *Article) {
			_, err := coll.DeleteOne(ctx, bson.M{"_id": art.ID})
			if err != nil {
				t.Errorf("Fail to delete article: %v", art)
			}
		}(art)
		savedArt := &Article{}
		if err := coll.FindOne(ctx, bson.M{"_id": art.ID}).Decode(savedArt); err != nil {
			t.Fatalf("Failed to fetch article: %v", err)
		}
		if !reflect.DeepEqual(savedArt, art) {
			t.Logf("Input:         %v", art)
			t.Logf("From database: %v", savedArt)
			t.Errorf("Article from database does not match input.")
		}
	})

	t.Run("FailsWithoutName", func(t *testing.T) {
		art := &Article{Stock: 20}
		if err := art.Save(ctx, database); err == nil {
			t.Fatal("Should have failed to save but succeeded.")
		}
	})
}
