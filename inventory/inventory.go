package inventory

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

const CollectionName = "inventory"

type Inventory struct {
	Articles []*Article `json:"inventory"`
}

type Article struct {
	ID         primitive.ObjectID `bson:"_id,omitempty"`
	ExternalID int64              `bson:"external_id,omitempty" json:"art_id,string"`
	Name       string             `bson:"name,omitempty"`
	Stock      int64              `bson:"stock,omitempty" json:",string"`
}

// LoadInventory loads inventory articles from json.
//
// Example json:
//	{"inventory": [
//   		{"art_id": "1", "name": "screw", "stock": "150"}
//	]}
//
// Returns the parsed inventory and optionally an error.
func LoadInventory(r io.Reader) (*Inventory, error) {
	inventory := &Inventory{}
	dec := json.NewDecoder(r)
	err := dec.Decode(inventory)
	if err != nil {
		return nil, err
	}
	return inventory, nil
}

func SaveMany(ctx context.Context, db *mongo.Database, arts []*Article) error {
	coll := db.Collection(CollectionName)

	elements := []interface{}{}
	for _, art := range arts {
		elements = append(elements, art)
	}

	res, err := coll.InsertMany(ctx, elements)
	if err != nil {
		return fmt.Errorf("Failed to save articles: %v", err)
	}
	for i, v := range res.InsertedIDs {
		arts[i].ID = v.(primitive.ObjectID)
	}
	return nil
}

func (art *Article) Save(ctx context.Context, db *mongo.Database) error {
	if art.Name == "" {
		return errors.New("Missing name in article.")
	}
	coll := db.Collection(CollectionName)
	res, err := coll.InsertOne(ctx, art)
	if err != nil {
		return fmt.Errorf("Failed to save article: %v", err)
	}
	art.ID = res.InsertedID.(primitive.ObjectID)
	return nil
}
