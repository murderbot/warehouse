module github.com/flaviamissi/warehouse

go 1.15

require (
	github.com/fatih/color v1.10.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/rakyll/gotest v0.0.5 // indirect
	github.com/sirupsen/logrus v1.7.0
	go.mongodb.org/mongo-driver v1.4.3
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68 // indirect
)
