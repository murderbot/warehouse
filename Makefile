VERSION := test

.PHONY: build
build:
	@docker build -t warehouse:${VERSION} -t warehouse:latest .

.PHONY: run
run:
	@docker-compose up -d

.PHONY: test
test:
	@go test ./...

.PHONY: db-seed
db-seed:
	@go run ./cmd

.PHONY: docs
docs:
	@echo "Visit http://localhost:6060/pkg/github.com/flaviamissi/warehouse/"
	@godoc -http=localhost:6060
