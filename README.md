# Build & Run

This project has a few dependencies, most notably [mongodb](https://www.mongodb.com/). The easiest way to run it is via docker. The instructions bellow will assume you have both [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/) installed.
To successfully start all necessary containers you'll need the following ports available:

* 8182
* 27017
* 8081

You can alternatively change the docker-compose.yml "ports" mapping and choose a different port, the second value is the host port.

```
$ make build run
```

# Seed the database

Load json files into the database:

```
make db-seed
```

This command is not idempotent!

# Questions/Assumptions

* What does it mean to sell a product, besides the required inventory update?
  * Should sale records be stored as its own thing?
  * An option is to record every inventory state change, similar to an event log
* My interpretation of the "Get all products [...]" requirement is that products are listed with the necessary articles, as well as how many of these articles are available in the inventory.

# Code Assignment Description

## Intro
This assignment will be used as a discussion during a technical interview.
The primary values for the code we look for are: simplicity, readability, maintainability, testability. It should be easy to scan the code, and rather quickly understand what it’s doing. Pay attention to naming.
 
You may choose any coding language, and we look forward to discussing your choice.

## The Task
The assignment is to implement a warehouse software. This software should hold articles, and the articles should contain an identification number, a name and available stock. It should be possible to load articles into the software from a file, see the attached inventory.json.
The warehouse software should also have products, products are made of different articles. Products should have a name, price and a list of articles of which they are made from with a quantity. The products should also be loaded from a file, see the attached products.json. 
 
The warehouse should have at least the following functionality;
* Get all products and quantity of each that is an available with the current inventory
* Remove(Sell) a product and update the inventory accordingly
