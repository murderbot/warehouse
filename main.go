package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"github.com/flaviamissi/warehouse/db"
	"github.com/flaviamissi/warehouse/inventory"
)

const dbURI = "mongodb://localhost"

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := db.Connect(ctx, dbURI)
	if err != nil {
		log.Error("Failed to setup db!")
		panic(err)
	}
	defer client.Disconnect(ctx)
	database := db.Database(client)

	// create database indexes
	coll := database.Collection(inventory.CollectionName)
	im := mongo.IndexModel{
		Keys:    bson.M{"external_id": 1},
		Options: options.Index().SetUnique(true).SetName("uniqueExternalId"),
	}
	if _, err := coll.Indexes().CreateOne(ctx, im); err != nil {
		log.Errorf("Failed to create index: %v", err)
	}

	r := mux.NewRouter()
	r.Use(loggingMiddleware)
	r.Use(db.MiddlewareFn(database))
	r.HandleFunc("/", http.HandlerFunc(handler))
	r.HandleFunc("/product/sell/{id:[a-f][0-9]+}",
		http.HandlerFunc(sellProduct)).Methods("PATCH")
	r.HandleFunc("/product",
		http.HandlerFunc(getProducts)).Methods("GET")

	srv := http.Server{
		Handler: r,
		Addr:    "0.0.0.0:8182",
	}

	idleConnsClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint

		// received an interrupt signal, shut down.
		if err := srv.Shutdown(ctx); err != nil {
			// error from closing listeners, or context timeout:
			log.Errorf("HTTP server Shutdown: %v", err)
		}
		close(idleConnsClosed)
	}()

	log.Info("Started server on :8182")
	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		// Error starting or closing listener:
		log.Fatalf("HTTP server ListenAndServe: %v", err)
	}

	<-idleConnsClosed
}

func handler(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	db := db.FromContext(ctx)
	article := inventory.Article{
		ExternalID: 1,
		Name:       "leg",
		Stock:      12,
	}
	res, err := db.Collection(inventory.CollectionName).InsertOne(ctx, article)
	if err != nil {
		log.Error(err)
		fmt.Fprintf(w, "Errored: %v", err)
	}
	fmt.Fprintf(w, "Result id: %v", res.InsertedID)
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		log.Infof("%s %s", req.Method, req.RequestURI)
		next.ServeHTTP(w, req)
	})
}
