package product

import (
	"context"
	"encoding/json"
	"fmt"
	"io"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/flaviamissi/warehouse/inventory"
)

const CollectionName = "products"

type products struct {
	Products []*Product
}

type Product struct {
	ID       primitive.ObjectID `bson:"_id,omitempty"`
	Articles []ProductArticle   `bson:"articles,omitempty" json:"contain_articles"`
	Name     string             `bson:"name,omitempty"`
}

type ProductArticle struct {
	ExternalID int64 `bson:"external_id,omitempty" json:"art_id,string"`
	Quantity   int64 `bson:"quantity,omitempty" json:"amount_of,string"`
}

// LoadProducts loads products from json.
//
// Example json:
//	{"products": [
//   		{"name": "Dining Chair", "contain_articles": [{"art_id": "1", "amount_of": 4}]}
//	]}
//
// Returns the parsed products and an optional error.
func LoadProducts(r io.Reader) ([]*Product, error) {
	var products products
	dec := json.NewDecoder(r)
	err := dec.Decode(&products)
	if err != nil {
		return nil, err
	}
	return products.Products, nil
}

func SaveMany(ctx context.Context, db *mongo.Database, products []*Product) error {
	coll := db.Collection(CollectionName)

	elements := []interface{}{}
	for _, p := range products {
		elements = append(elements, p)
	}

	res, err := coll.InsertMany(ctx, elements)
	if err != nil {
		return fmt.Errorf("Failed to save articles: %v", err)
	}
	for i, v := range res.InsertedIDs {
		products[i].ID = v.(primitive.ObjectID)
	}
	return nil
}

func (p *Product) Save(ctx context.Context, db *mongo.Database) error {
	coll := db.Collection(CollectionName)
	res, err := coll.InsertOne(ctx, p)
	p.ID = res.InsertedID.(primitive.ObjectID)
	return err
}

func (p *Product) Sell(ctx context.Context, db *mongo.Database) error {
	invColl := db.Collection(inventory.CollectionName)

	extIDs := make([]int64, len(p.Articles))
	for _, art := range p.Articles {
		extIDs = append(extIDs, art.ExternalID)
	}

	filter := bson.M{"external_id": bson.M{"$in": extIDs}}
	cur, err := invColl.Find(ctx, filter)
	if err != nil {
		return fmt.Errorf("Required article(s) not found: %v", err)
	}
	invArt := make([]*inventory.Article, len(p.Articles))
	if err := cur.All(ctx, &invArt); err != nil {
		return fmt.Errorf("Could not parse articles: %v", err)
	}

	productArtsByEID := mapArticles(p.Articles)

	// TODO: execute below operations within transaction
	for _, art := range invArt {
		prodArt := productArtsByEID[art.ExternalID]
		newQty := art.Stock - prodArt.Quantity
		if newQty < 0 {
			return fmt.Errorf("Insufficient availability for article '%s'.", art.Name)
		}

		res, err := invColl.UpdateOne(
			ctx,
			bson.M{"_id": art.ID},
			bson.D{{"$set", bson.D{{"stock", newQty}}}},
		)
		if err != nil {
			return fmt.Errorf("Failed to update article '%s': %v.", art.ID, err)
		}
		if res.ModifiedCount == 0 {
			return fmt.Errorf("Failed to update article '%s'('%s').", art.Name, art.ID)
		}
	}

	return nil
}

func (pa ProductArticle) GetExternalID() int64 {
	return pa.ExternalID
}

func mapArticles(articles []ProductArticle) map[int64]ProductArticle {
	arts := make(map[int64]ProductArticle, len(articles))
	for _, art := range articles {
		arts[art.ExternalID] = art
	}
	return arts
}
