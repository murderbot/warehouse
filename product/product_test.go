package product

import (
	"context"
	"os"
	"reflect"
	"strings"
	"testing"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/flaviamissi/warehouse/db"
	"github.com/flaviamissi/warehouse/inventory"
)

func TestLoadProducts(t *testing.T) {
	t.Run("WithOneProductAndArticle", func(t *testing.T) {
		d := `
		{"products": [
			{"name": "Dining Chair", "contain_articles": [{"art_id": "1", "amount_of": "4"}]}
		]}`
		products, err := LoadProducts(strings.NewReader(d))
		if err != nil {
			t.Fatalf("Could not load products: %v", err)
		}
		if len(products) != 1 {
			t.Fatalf("Number of parsed products is not same as input. Want 1, got '%d'.",
				len(products))
		}
		p := products[0]
		if p.Name != "Dining Chair" {
			t.Errorf("Wrong product name, want 'Dining Chair', got '%s'", p.Name)
		}
		art := p.Articles[0]
		if art.ExternalID != 1 {
			t.Errorf("Wrong article id, want '1', got '%d'", art.ExternalID)
		}
		if art.Quantity != 4 {
			t.Errorf("Wrong quantity, want '4', got '%d'", art.Quantity)
		}
	})

	t.Run("WithOneProductAndMultipleArticles", func(t *testing.T) {
		d := `
		{"products": [
			{
				"name": "Dining Table",
				"contain_articles": [
					{"art_id": "1", "amount_of": "3"},
					{"art_id": "4", "amount_of": "13"}
				]
			}
		]}`
		products, err := LoadProducts(strings.NewReader(d))
		if err != nil {
			t.Fatalf("Could not load products: %v", err)
		}
		if len(products) != 1 {
			t.Fatalf("Number of parsed products is not same as input. Want 1, got '%d'.",
				len(products))
		}
		p := products[0]
		if p.Name != "Dining Table" {
			t.Errorf("Wrong product name, want 'Dining Table', got '%s'", p.Name)
		}
		art := p.Articles[0]
		if art.ExternalID != 1 {
			t.Errorf("Wrong article id, want '1', got '%d'", art.ExternalID)
		}
		if art.Quantity != 3 {
			t.Errorf("Wrong quantity, want '4', got '%d'", art.Quantity)
		}
		art = p.Articles[1]
		if art.ExternalID != 4 {
			t.Errorf("Wrong article id, want '4', got '%d'", art.ExternalID)
		}
		if art.Quantity != 13 {
			t.Errorf("Wrong quantity, want '13', got '%d'", art.Quantity)
		}
	})

	t.Run("FromFile", func(t *testing.T) {
		f, err := os.Open("../products.json")
		if err != nil {
			t.Fatalf("Could not open products json: %v", err)
		}
		defer f.Close()
		products, err := LoadProducts(f)
		if err != nil {
			t.Fatalf("Could not load products: %v", err)
		}
		if len(products) != 2 {
			t.Fatalf("Wrong number of products parsed, want '2', got '%d'", len(products))
		}

		p := products[0]
		if p.Name != "Dining Chair" {
			t.Errorf("Product has wrong name, want 'Dining Chair', got '%s'", p.Name)
		}
		if len(p.Articles) != 3 {
			t.Fatalf("Wrong number of articles, want '3', got '%d'", len(p.Articles))
		}
		articles := []ProductArticle{
			{ExternalID: 1, Quantity: 4},
			{ExternalID: 2, Quantity: 8},
			{ExternalID: 3, Quantity: 1},
		}
		if !reflect.DeepEqual(articles, p.Articles) {
			t.Logf("Parsed articles: %v", p.Articles)
			t.Errorf("Parsed articles don't match input.")
		}

		p = products[1]
		if p.Name != "Dinning Table" {
			t.Errorf("Product has wrong name, want 'Dining Table', got '%s'", p.Name)
		}
		if len(p.Articles) != 3 {
			t.Fatalf("Wrong number of articles, want '3', got '%d'", len(p.Articles))
		}
		articles = []ProductArticle{
			{ExternalID: 1, Quantity: 4},
			{ExternalID: 2, Quantity: 8},
			{ExternalID: 4, Quantity: 1},
		}
		if !reflect.DeepEqual(articles, p.Articles) {
			t.Logf("Parsed articles: %v", p.Articles)
			t.Errorf("Parsed articles don't match input.")
		}
	})
}

func TestSaveMany(t *testing.T) {
	ctx := context.Background()
	client, err := db.Connect(ctx, "mongodb://localhost")
	if err != nil {
		t.Log("Failed to setup db!")
		t.Fatal(err)
	}
	defer client.Disconnect(ctx)
	database := db.Database(client)
	coll := database.Collection(CollectionName)

	t.Run("WithMultipleArticles", func(t *testing.T) {
		f, err := os.Open("../products.json")
		if err != nil {
			t.Errorf("Failed to open products json: %v", err)
		}
		defer f.Close()
		products, err := LoadProducts(f)
		if err != nil {
			t.Errorf("Failed to load products json: %v", err)
		}
		if err := SaveMany(ctx, database, products); err != nil {
			t.Fatalf("Failed to save products: %v", err)
		}
		ids := []primitive.ObjectID{}
		for _, p := range products {
			ids = append(ids, p.ID)
		}
		filter := bson.M{"_id": bson.M{"$in": ids}}
		defer coll.DeleteMany(ctx, filter)

		for _, p := range products {
			if p.ID.IsZero() {
				t.Fatalf("Object ID was empty: '%s'", p.ID.String())
			}
			if p.Name == "" {
				t.Fatalf("Product name was empty: %v", p)
			}
			if len(p.Articles) == 0 {
				t.Fatal("Product has no articles")
			}
		}
	})
}

func TestSaveProduct(t *testing.T) {
	ctx := context.Background()
	client, err := db.Connect(ctx, "mongodb://localhost")
	if err != nil {
		t.Fatalf("Failed connecting to database: %v", err)
	}
	defer client.Disconnect(ctx)
	database := db.Database(client)
	coll := database.Collection(CollectionName)

	t.Run("WithNoArticles", func(t *testing.T) {
		p := &Product{
			Name: "Chair",
		}
		if err := p.Save(ctx, database); err != nil {
			t.Fatalf("Failed to save product: %v", err)
		}
		defer coll.DeleteOne(ctx, bson.M{"_id": p.ID})

		var newP *Product
		if err := coll.FindOne(ctx, bson.M{"_id": p.ID}).Decode(&newP); err != nil {
			t.Fatalf("Could not fetch product from database: %v", err)
		}
		if p.ID != newP.ID {
			t.Errorf("Product IDs don't match, want '%s' but got '%s'",
				newP.ID.String(), p.ID.String())
		}
	})

	t.Run("WithMultipleArticles", func(t *testing.T) {
		f, err := os.Open("../products.json")
		if err != nil {
			t.Errorf("Failed to open products json: %v", err)
		}
		defer f.Close()
		products, err := LoadProducts(f)
		if err != nil {
			t.Errorf("Failed to load products json: %v", err)
		}
		ids := make([]primitive.ObjectID, len(products))
		for _, p := range products {
			if err := p.Save(ctx, database); err != nil {
				t.Fatalf("Could not save product: %v", err)
			}
			if p.ID.IsZero() {
				t.Fatalf("Object ID was empty: '%s'", p.ID.String())
			}
			ids = append(ids, p.ID)
		}
		defer coll.DeleteMany(ctx, bson.M{"_id": bson.M{"$in": ids}})
	})
}

func TestSellProduct(t *testing.T) {
	ctx := context.Background()
	client, err := db.Connect(ctx, "mongodb://localhost")
	if err != nil {
		t.Fatalf("Failed connecting to database: %v", err)
	}
	defer client.Disconnect(ctx)
	database := db.Database(client)
	artColl := database.Collection(inventory.CollectionName)

	var (
		leg_stock   int64 = 6
		screw_stock int64 = 10
		seat_stock  int64 = 5
	)
	if err := inventory.SaveMany(ctx, database,
		[]*inventory.Article{
			{Name: "leg", ExternalID: 1, Stock: leg_stock},
			{Name: "screw", ExternalID: 2, Stock: screw_stock},
			{Name: "seat", ExternalID: 4, Stock: seat_stock},
		},
	); err != nil {
		t.Fatalf("Could not save articles: %v", err)
	}

	p := &Product{
		Name: "Chair",
		Articles: []ProductArticle{
			{ExternalID: 1, Quantity: 4},
			{ExternalID: 2, Quantity: 8},
			{ExternalID: 4, Quantity: 1},
		},
	}
	if err := p.Save(ctx, database); err != nil {
		t.Fatalf("Failed to save product: %v", err)
	}

	if err := p.Sell(ctx, database); err != nil {
		t.Fatalf("Failed to sell product: %v", err)
	}
	defer database.Collection(CollectionName).DeleteOne(ctx, bson.M{"_id": p.ID})

	artIDs := make([]int64, len(p.Articles))
	for _, article := range p.Articles {
		artIDs = append(artIDs, article.ExternalID)
	}
	defer artColl.DeleteMany(ctx, bson.M{"external_id": bson.M{"$in": artIDs}})

	articles := make([]*inventory.Article, len(p.Articles))
	cur, err := artColl.Find(ctx, bson.M{"external_id": bson.M{"$in": artIDs}})
	if err != nil {
		t.Fatalf("Could not fetch articles from database: %v", err)
	}
	if err := cur.All(ctx, &articles); err != nil {
		t.Fatalf("Could not fetch product from database: %v", err)
	}
	if err := cur.Err(); err != nil {
		t.Fatalf("Error in cursor: %v", err)
	}

	productArtsByEID := mapArticles(p.Articles)
	for _, art := range articles {
		prodArt := productArtsByEID[art.ExternalID]
		switch art.Name {
		case "leg":
			newQty := leg_stock - prodArt.Quantity
			if art.Stock != newQty {
				t.Logf("Stock:     %d", art.Stock)
				t.Logf("Quantity:  %d", prodArt.Quantity)
				t.Logf("New stock: %d", newQty)
				t.Errorf("Stock for article '%s' did not decrease after sell", art.Name)
			}
		case "screw":
			newQty := screw_stock - prodArt.Quantity
			if art.Stock != newQty {
				t.Logf("Stock:     %d", art.Stock)
				t.Logf("Quantity:  %d", prodArt.Quantity)
				t.Logf("New stock: %d", newQty)
				t.Errorf("Stock for article '%s' did not decrease after sell", art.Name)
			}
		case "seat":
			newQty := seat_stock - prodArt.Quantity
			if art.Stock != newQty {
				t.Logf("Stock:     %d", art.Stock)
				t.Logf("Quantity:  %d", prodArt.Quantity)
				t.Logf("New stock: %d", newQty)
				t.Errorf("Stock for article '%s' did not decrease after sell", art.Name)
			}
		}
	}
}
