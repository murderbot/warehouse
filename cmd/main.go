package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/flaviamissi/warehouse/db"
	"github.com/flaviamissi/warehouse/inventory"
	"github.com/flaviamissi/warehouse/product"
)

func main() {
	invPath := flag.String("inventory", "inventory.json",
		"Path to inventory json, as documented by inventory.LoadInventory")
	productsPath := flag.String("products", "products.json",
		"Path to products json, as documented by product.LoadProducts")
	flag.Parse()

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := db.Connect(ctx, "mongodb://localhost")
	if err != nil {
		log.Error("Failed to setup db!")
		panic(err)
	}
	defer client.Disconnect(ctx)
	database := db.Database(client)

	fmt.Printf("Creating inventory articles from '%s'... ", *invPath)
	f, err := os.Open(*invPath)
	if err != nil {
		fmt.Printf("Failed to open inventory: %v\n", err)
		os.Exit(1)
	}
	defer f.Close()
	inv, err := inventory.LoadInventory(f)
	if err != nil {
		fmt.Printf("Failed to load inventory: %v\n", err)
		os.Exit(1)
	}
	if err := inventory.SaveMany(ctx, database, inv.Articles); err != nil {
		fmt.Printf("Failed to save inventory: %v\n", err)
		os.Exit(1)
	}
	fmt.Println("Done")

	fmt.Printf("Creating products from '%s'... ", *productsPath)
	f, err = os.Open(*productsPath)
	if err != nil {
		fmt.Printf("Failed to open inventory: %v\n", err)
		os.Exit(1)
	}
	defer f.Close()
	products, err := product.LoadProducts(f)
	if err != nil {
		fmt.Printf("Failed to load inventory: %v\n", err)
		os.Exit(1)
	}
	if err := product.SaveMany(ctx, database, products); err != nil {
		fmt.Printf("Failed to save inventory: %v\n", err)
		os.Exit(1)
	}
	fmt.Println("Done")
}
